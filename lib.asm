global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global read_string
global parse_uint
global parse_int
global string_copy
global exit

exit: 
    mov rax, 60
    syscall

string_length:
    xor rax, rax
    .while:
        test byte[rdi], 0FFh
        jz .end
        inc rdi
        inc rax
        jmp .while
    .end:
        ret

print_string:
	push rdi
    push rsi
    call string_length
    pop rdi
	pop rsi
    mov rdx, rax
    mov rax, 1
    syscall
    ret

print_char:
    push rdi
    mov rax, 1
    mov rdi, 1
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rdi
    ret

print_newline:
    mov rdi, 10
    call print_char
    ret

print_uint:
    mov rax, rdi
    mov rdi, 10
    xor rcx, rcx
    
    .while:
        xor rdx, rdx
        div rdi
        add rdx, '0'
        dec rsp
        mov [rsp], dl
        inc rcx
        test rax, rax
        jnz .while
    
    mov rax, 1
    mov rdi, 1
    mov rsi, rsp
    add rsp, rcx
    mov rdx, rcx
    syscall
    
    ret

print_int:
    test rdi, rdi
    jns .plus
    
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
    
    .plus:
    call print_uint
    ret

string_equals:
    .while:
        mov dl, [rdi]
        xor dl, [rsi]
        jnz .unequal
        test byte [rdi], 0FFh
        jz .finish
        inc rdi
        inc rsi
        jmp .while
    .unequal:
        xor rax, rax
        ret
    .finish:
		mov rax, 1
        ret

read_char:
    dec rsp
    xor rax, rax
    xor rdi, rdi
    mov rsi, rsp
    mov rdx, 1
    syscall
    inc rsp

    test rax, rax
    jz .finish
    mov al, [rsp - 1]
    .finish: ret 

read_char_and_join_spaces:
    call read_char
    cmp rax, 9h
    je .space
    cmp rax, 0Ah
    je .space
    ret
    .space:
    mov rax, 20h
    ret

read_word:
    push rsi
    test rsi, rsi
    jz .err
    
    push 0
    push rdi
    
    .skip_spaces:
        call read_char_and_join_spaces
        test rax, rax
        jz .finish
        cmp rax, 20h
        je .skip_spaces
    
    pop rdi
    pop rdx
    
    mov [rdi], al
    inc rdx
    cmp rdx, [rsp]
    je .err
    
    .read:
        push rdx
        push rdi
        call read_char_and_join_spaces
        test rax, rax
        jz .finish
        cmp rax, 20h
        je .finish
        
        pop rdi
        pop rdx
        mov [rdi + rdx], al
        inc rdx
        cmp rdx, [rsp]
        jne .read
    
    .err:
        pop rsi
        xor rax, rax
        ret
    
    .finish:
        pop rdi
        pop rdx
        pop rsi
        mov byte[rdi + rdx], 0
        mov rax, rdi
        ret

read_string:
	test rsi, rsi
	jz .ret_zero
	
	push rsi
	xor rdx, rdx
	
	.read:
		push rdx
		push rdi
		call read_char
		pop rdi
		pop rdx
		
		test rax, rax
		jz .finish
		cmp rax, 0Ah
		je .finish
		
		mov [rdi + rdx], al
		inc rdx
		cmp rdx, [rsp]
		jne .read
	
	.err:
		pop rsi
	.ret_zero:
		xor rax, rax
		ret
	
	.finish:
		pop rsi
		mov byte[rdi + rdx], 0
		mov rax, rdi
		ret

parse_uint:
        xor rax, rax
        mov al, [rdi]
        cmp al, '0'
        jl .err
        cmp al, '9'
        jg .err
        
        mov rdx, 1
        sub al, '0'
        mov rsi, 10
        xor rcx, rcx
    
    .while:
        mov cl, [rdi + rdx]
        cmp cl, '0'
        jl .finish
        cmp cl, '9'
        jg .finish
        
        inc rdx
        
        push rdx
        mul rsi
        pop rdx
        
        sub cl, '0'
        add rax, rcx
        
        jmp .while
    
    .err:    xor rdx, rdx
    .finish:    ret

parse_int:
		cmp byte[rdi], '-'
		jz .negative
		call parse_uint
		ret
	.negative:
		inc rdi
		call parse_uint
		test rdx, rdx
		jz .err
		inc rdx
		neg rax
		ret
	.err:
		xor rax, rax
		ret

string_copy:
    xor rax, rax
    .while:
        cmp rax, rdx
        je .err
        mov cl, [rdi + rax]
        mov [rsi + rax], cl
        test cl, cl
        jz .finish
        inc rax
        jmp .while
        
    .err:
        xor rax, rax
    .finish:
        ret