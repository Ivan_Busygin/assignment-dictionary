%assign i 0
%xdefine addr 0

%macro colon 2
	%xdefine new_addr label %+ i
	
	new_addr: dq addr
	db %1, 0
	%2:
	
	%xdefine addr new_addr
	%assign i  i + 1
%endmacro