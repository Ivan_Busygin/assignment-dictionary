extern string_equals
global find_word
global get_value

; Принимает указатель на нуль-терминированную строку-ключ (rdi) и указатель на начало словаря (rsi).
; Возвращает адрес начала вхождения в словарь (не значения) или ноль, если вхождения нет.
find_word:
	.while:
		test rsi, rsi
		jz .finish
		
		push rdi
		push rsi
		add rsi, 8
		call string_equals
		pop rsi
		pop rdi
		
		cmp rax, 1
		je .finish
		
		mov rsi, [rsi]
		jmp .while
	
	.finish:
		mov rax, rsi
		ret

; Принимает адрес начала вхождения в словарь (rdi).
; Возвращает адрес ячейки, следующей за строковым ключём (данные по ключу).
get_value:
	mov rax, rdi
	add rax, 8
	
	.while:
		mov bl, [rax]
		test bl, bl
		jz .break
		inc rax
		jmp .while
	.break:
	
	inc rax
	ret