AS = nasm
LD = ld
AS_FLAGS = -f elf64

program: main.o dict.o lib.o
	$(LD) -o $@ $^
 
%.o: %.asm
	$(AS) $(AS_FLAGS) -o $@ -g $<

clean:
	rm -f *.o

help:
	echo 'Target list: help, clean, program, lib.o, main.o, dict.o.'

.PHONY: clean help