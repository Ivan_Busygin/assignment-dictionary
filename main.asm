extern find_word
extern get_value
%include "lib.inc"

global _start

%define STR_SIZE 256
%define STDOUT 1
%define STDERR 2

section .data
	%include "words.inc"
	too_large_line: db "Слишком большая строка. Должно быть не более 255 символов.", 10, 0
	not_found: db "В списке нет записи с таким ключом.", 10, 0
	
section .text
_start:
	sub rsp, STR_SIZE
	
	mov rdi, rsp
	mov rsi, STR_SIZE
	call read_string
	
	test rax, rax
	jz .too_large_line
	
	mov rdi, rax
	mov rsi, addr
	call find_word
	
	add rsp, STR_SIZE
	
	test rax, rax
	jz .not_found
	
	mov rdi, rax
	call get_value
	
	mov rdi, rax
	mov rsi, STDOUT
	call print_string
	
	call print_newline
	
	mov rdi, 0
	call exit
	
	.too_large_line:
		mov rdi, too_large_line
		mov rsi, STDERR
		call print_string
		jmp .err
	.not_found:
		mov rdi, not_found
		mov rsi, STDERR
		call print_string
	.err:
		mov rdi, 1
		call exit